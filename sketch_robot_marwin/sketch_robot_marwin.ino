

#include "packet_handler.h"

const uint8_t motor_pwms[] = {3, 5, 6, 9};
const uint8_t motor_dirs[] = {2, 4, 7, 8};
const uint8_t motor_enca[] = {12, A4, 10, A2};
const uint8_t motor_encb[] = {13, A5, A1, A3};

uint8_t motor_enc_prev[] = {0, 0, 0, 0};

int32_t motor_enc[] = {0, 0, 0, 0};

const uint8_t weapon_dir = A0;
const uint8_t weapon_pwm = 11;

const uint8_t battery_pin = A6;
const uint8_t distance_pin = A7;

uint32_t last_tick = 0;

const int8_t trans_table[] =
{
   0,  1, -1,  0,
  -1,  0,  0,  1,
   1,  0,  0, -1,
   0, -1,  1,  0
};

Deserializer g_xParser;
Frame g_xTxframe;



void send_frame(Frame &frame, uint8_t frame_type)
{
  frame.fill_header(frame_type);
  Serial.write((uint8_t*) &frame, frame.header.length);
}


void setup()
{
  Serial.begin(115200);
  
  for(int i=0; i<4; i++)
  {
    pinMode(motor_dirs[i], OUTPUT); 
    pinMode(motor_pwms[i], OUTPUT); 
    pinMode(motor_enca[i], INPUT);
    pinMode(motor_encb[i], INPUT);
  }

  pinMode(weapon_dir, OUTPUT);
  pinMode(weapon_pwm, OUTPUT);
}

void loop()
{
  for(int i=0; i<4; i++)
  {
    motor_enc_prev[i] <<= 2;
    motor_enc_prev[i] |= digitalRead(motor_enca[i])<<1;
    motor_enc_prev[i] |= digitalRead(motor_encb[i]);
    motor_enc_prev[i] &= 0x0f;

    motor_enc[i] += (int32_t) trans_table[motor_enc_prev[i]];
  }

  if(Serial.available() > 0)
  {
    if(g_xParser.add_byte(Serial.read()))
    {
      Frame *rx_frame = g_xParser.get_frame();

      switch(rx_frame->header.message_type)
      {
      case PacketType_ReadBattery:
        g_xTxframe.report_battery.voltage = analogRead(battery_pin) * 10;
        send_frame(g_xTxframe, PacketType_ReportBattery);
        break;
      case PacketType_SetMotorSpeed:
        for(int i=0; i<4; i++)
        {
          digitalWrite(motor_dirs[i], rx_frame->set_motor_speed.motor_dirs[i]);
          analogWrite(motor_pwms[i], rx_frame->set_motor_speed.motor_pwms[i]);
        }
        break;
      case PacketType_SetWeaponSpeed:
        digitalWrite(weapon_dir, rx_frame->set_weapon_speed.dir);
        analogWrite(weapon_pwm, rx_frame->set_weapon_speed.pwm);
        break;
      }

      g_xParser.clear();
    }
  }

  if( (millis() - last_tick) < 100 )
    return;

  last_tick += 100;

  // Send the period encoder update
  for(uint8_t i; i<4; i++)
    g_xTxframe.report_encoder.encoders[i] = motor_enc[i];
  send_frame(g_xTxframe, PacketType_ReportEncoder);

  // Read and report the voltage on the sharp distance sensor
  g_xTxframe.report_distance_voltage.voltage = analogRead(distance_pin) * 10;
  send_frame(g_xTxframe, PacketType_ReportDistanceVoltage);
}

