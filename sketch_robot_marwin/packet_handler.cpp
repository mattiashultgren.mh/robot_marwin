
#include <stdint.h>

#include "packet_handler.h"


// Local definitions
static uint8_t packet_seq_num = 0;
static uint16_t calc_chksum(uint8_t *data, uint8_t size);


static uint16_t calc_chksum(uint8_t *data, uint8_t size)
{
  uint16_t reg = 0xffff;

  while(size > 0)
  {
    size--;
    reg += reg << 8;
    reg += (uint16_t) *(data++);
  }

  return reg;
}


bool Deserializer::add_byte(uint8_t data)
{
  buffer[buffer_index] = data;
  
  if(buffer_index == 0)
  {
    if(data == START_BYTE)
      buffer_index++;
  }
  else if(buffer_index == 1)
  {
    expected_size = data;
    buffer_index++;
    if(expected_size > MAX_PACKET_SIZE)
    {
      buffer_index = 0;
    }
  }
  else
    buffer_index++;

  if(buffer_index >= MIN_PACKET_SIZE  &&  buffer_index == expected_size)
  {
    uint16_t chksum = *((uint16_t*)(buffer+buffer_index-2));
    if(chksum == calc_chksum(buffer, expected_size-2))
    {
      if(frame.header.length == Frame::get_size(frame.header.message_type))
        return true;
      else
      {
        buffer_index = 0;
        return false;
      }
    }
    else
    {
      buffer_index = 0;
      return false;
    }
  }
  else
    return false;
}


uint8_t Frame::get_size(uint8_t frame_type)
{
  uint8_t frame_size = sizeof(FrameHeader) + sizeof(uint16_t);

  switch(frame_type)
  {
  case PacketType_ReadBattery:
    break;
  case PacketType_ReportBattery:
  case PacketType_ReportDistanceVoltage:
    frame_size += sizeof(ReportVoltage);
    break;
  case PacketType_ReportEncoder:
    frame_size += sizeof(ReportEncoder);
    break;
  case PacketType_SetMotorSpeed:
    frame_size += sizeof(SetMotorSpeed);
    break;
  case PacketType_SetWeaponSpeed:
    frame_size += sizeof(SetWeaponSpeed);
    break;
  }

  return frame_size;
}


void Frame::fill_header(uint8_t frame_type)
{
  uint16_t checksum;
  header.start_byte = START_BYTE;
  header.length = get_size(frame_type);
  header.sequence_number = packet_seq_num++;
  header.message_type = frame_type;
  
  checksum = calc_chksum((uint8_t*) this, header.length-sizeof(uint16_t));
  *((uint16_t*) (((uint8_t*) this) + header.length-sizeof(uint16_t))) = checksum;
}

