
#ifndef PACKET_HANDLER_H_
#define PACKET_HANDLER_H_

#include <stdint.h>

const int MIN_PACKET_SIZE = 6;
const int MAX_PACKET_SIZE = 32;
const uint8_t START_BYTE = 0xC4;

const uint8_t PacketType_ReadBattery = 0x00;
const uint8_t PacketType_ReportBattery = 0x01;
const uint8_t PacketType_ReportEncoder = 0x02;
const uint8_t PacketType_SetMotorSpeed = 0x03;
const uint8_t PacketType_SetWeaponSpeed = 0x04;
const uint8_t PacketType_ReportDistanceVoltage = 0x05;

struct __attribute__((packed)) ReadBattery
{
};
struct __attribute__((packed)) ReportVoltage
{
    uint16_t voltage;
};
struct __attribute__((packed)) ReportEncoder
{
    int32_t encoders[4];
};
struct __attribute__((packed)) SetMotorSpeed
{
    uint8_t motor_pwms[4];
    uint8_t motor_dirs[4];
};
struct __attribute__((packed)) SetWeaponSpeed
{
    uint8_t pwm;
    uint8_t dir;
};

struct __attribute__((packed)) FrameHeader
{
    uint8_t start_byte;
    uint8_t length;
    uint8_t sequence_number;
    uint8_t message_type;
};
struct __attribute__((packed)) Frame
{
    FrameHeader header;
    union
    {
        ReadBattery read_battery;
        ReportVoltage report_battery;
        ReportEncoder report_encoder;
        SetMotorSpeed set_motor_speed;
        SetWeaponSpeed set_weapon_speed;
        ReportVoltage report_distance_voltage;
    };
    uint16_t dummy; // just to make sure that the crc fits in the frame

    void fill_header(uint8_t frame_type);
    static uint8_t get_size(uint8_t frame_type);
};


class Deserializer
{
public:
    

    Deserializer() : buffer_index(0),
                     expected_size(0),
                     checksum(0)
    {
    }

    bool add_byte(uint8_t data);
    
    const Frame * get_frame() const { return &frame; }

    uint8_t operator[](uint8_t index) const
    {
      return buffer[index];
    }

    void clear(void)
    {
      buffer_index = 0;
    }

private:
    union
    {
        uint8_t buffer[MAX_PACKET_SIZE];
        Frame frame;
    };
    uint8_t buffer_index;
    uint8_t expected_size;
    uint16_t checksum;
};

#endif

