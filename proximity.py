from enum import IntEnum
import collections

import smbus


ProximityData = collections.namedtuple('ProximtyData', 'status_reg status range_mm')


class Reg(IntEnum):
    IdentificationModel = 0x000

    SysInterruptClear = 0x015

    SysRangeStart = 0x018

    SysAlsStart = 0x038  # ALS = ambient light sensor

    ResultRangeStatus = 0x04D
    ResultAlsStatus = 0x04E
    ResultInterruptStatusGpio = 0x04F
    ResultAlsValue16bit = 0x050

    ResultRangeValue = 0x062
    ResultRangeRaw = 0x064
    ResultRangeReturnRate = 0x066  # 9.7 format in Mcps


class RegIdentificationModel(IntEnum):
    Value = 0xB4


class RegSysInterruptClear(IntEnum):
    BitClearRange = 0x01
    BitClearAls = 0x02
    BitClearError = 0x04


class RegSysRangeStart(IntEnum):
    BitModeContinuous = 0x02  # Clearing this bit means single shot mode
    BitStartStop = 0x01  # Toggle on off by setting this bit


class RegSysAlsStart(IntEnum):
    BitModeContinuous = 0x02  # Clearing this bit means single shot mode
    BitStartStop = 0x01  # Toggle on off by setting this bit


class RegResultRangeStatus(IntEnum):
    BitDeviceReady = 0x01
    MaskError = 0xF0


class RegResultRangeStatusError(IntEnum):
    ErrorNoError = 0x00
    ErrorVcselContinuityTest = 0x10
    ErrorVcselWatchdogTest = 0x20
    ErrorVcselWatchdog = 0x30
    ErrorPLL1Lock = 0x40
    ErrorPLL2Lock = 0x50
    ErrorEarlyConvergenceEstimate = 0x60
    ErrorMaxConvergence = 0x70
    ErrorNoTargetIgnore = 0x80
    ErrorNotUsed = 0x90
    ErrorNotUsed2 = 0xA0
    ErrorMaxSignalToNoiseRatio = 0xB0
    ErrorRawRangingAlgoUnderflow = 0xC0
    ErrorRawRangingAlgoOverflow = 0xD0
    ErrorRangingAlgoUnderflow = 0xE0
    ErrorRangingAlgoOverflow = 0xF0


class RegResultAlsStatus(IntEnum):
    BitDeviceReady = 0x01
    MaskError = 0xF0
    ErrorNoError = 0x00


class RegResultInterruptStatusGpio(IntEnum):
    MaskIntRangeGpio = 0x07
    IRG_NoEvent = 0x00
    IRG_LevelLow = 0x01
    IRG_LevelHigh = 0x02
    IRG_OutOfWindow = 0x03
    IRG_NewSampleReady = 0x04


class Proximity:
    def __init__(self, bus, address=0x29):
        self.chip_name = 'VL6180X'
        self.address = address
        self.bus = bus

    def setup(self):
        id = self.bus.read_byte_data(self.address, Reg.IdentificationModel)
        if id != RegIdentificationModel.Value:
            print('Failed to read id register, expected', RegIdentificationModel.Value, 'but got', id)

        self.load_settings()

    def start_range_measurement(self):
        self.bus.write_byte_data(self.address, Reg.SysRangeStart, RegSysRangeStart.BitStartStop)

    def clear_interrupts(self):
        self.bus.write_byte_data(self.address, Reg.SysInterruptClear,
                                 RegSysInterruptClear.BitClearRange |
                                 RegSysInterruptClear.BitClearAls |
                                 RegSysInterruptClear.BitClearError)
        
    def load_settings(self):
        # private settings from page 24 of app note
        self.bus.write_byte_data(self.address, 0x0207, 0x01)
        self.bus.write_byte_data(self.address, 0x0208, 0x01)
        self.bus.write_byte_data(self.address, 0x0096, 0x00)
        self.bus.write_byte_data(self.address, 0x0097, 0xfd)
        self.bus.write_byte_data(self.address, 0x00e3, 0x00)
        self.bus.write_byte_data(self.address, 0x00e4, 0x04)
        self.bus.write_byte_data(self.address, 0x00e5, 0x02)
        self.bus.write_byte_data(self.address, 0x00e6, 0x01)
        self.bus.write_byte_data(self.address, 0x00e7, 0x03)
        self.bus.write_byte_data(self.address, 0x00f5, 0x02)
        self.bus.write_byte_data(self.address, 0x00d9, 0x05)
        self.bus.write_byte_data(self.address, 0x00db, 0xce)
        self.bus.write_byte_data(self.address, 0x00dc, 0x03)
        self.bus.write_byte_data(self.address, 0x00dd, 0xf8)
        self.bus.write_byte_data(self.address, 0x009f, 0x00)
        self.bus.write_byte_data(self.address, 0x00a3, 0x3c)
        self.bus.write_byte_data(self.address, 0x00b7, 0x00)
        self.bus.write_byte_data(self.address, 0x00bb, 0x3c)
        self.bus.write_byte_data(self.address, 0x00b2, 0x09)
        self.bus.write_byte_data(self.address, 0x00ca, 0x09)
        self.bus.write_byte_data(self.address, 0x0198, 0x01)
        self.bus.write_byte_data(self.address, 0x01b0, 0x17)
        self.bus.write_byte_data(self.address, 0x01ad, 0x00)
        self.bus.write_byte_data(self.address, 0x00ff, 0x05)
        self.bus.write_byte_data(self.address, 0x0100, 0x05)
        self.bus.write_byte_data(self.address, 0x0199, 0x05)
        self.bus.write_byte_data(self.address, 0x01a6, 0x1b)
        self.bus.write_byte_data(self.address, 0x01ac, 0x3e)
        self.bus.write_byte_data(self.address, 0x01a7, 0x1f)
        self.bus.write_byte_data(self.address, 0x0030, 0x00)

        # Recommended : Public registers - See data sheet for more detail
        self.bus.write_byte_data(self.address, 0x0011, 0x10)  # Enables polling for 'New Sample ready' when measurement
        # completes
        self.bus.write_byte_data(self.address, 0x010a, 0x30)  # Set the averaging sample period (compromise between
        # lower noise and increased execution time)
        self.bus.write_byte_data(self.address, 0x003f, 0x46)  # Sets the light and dark gain (upper nibble). Dark gain
        # should not be changed.
        self.bus.write_byte_data(self.address, 0x0031, 0xFF)  # sets the # of range measurements after which auto
        # calibration of system is performed
        self.bus.write_byte_data(self.address, 0x0040, 0x63)  # Set ALS integration time to 100ms
        self.bus.write_byte_data(self.address, 0x002e, 0x01)  # perform a single temperature calibration of the ranging
        # sensor

        # Optional: Public registers - See data sheet for more detail
        self.bus.write_byte_data(self.address, 0x001b, 0x09)  # Set default ranging inter-measurement period to 100ms
        self.bus.write_byte_data(self.address, 0x003e, 0x31)  # Set default ALS inter-measurement period to 500ms
        self.bus.write_byte_data(self.address, 0x0014, 0x24)  # Configures interrupt on 'New Sample Ready threshold event'

    def read_values(self):
        status = self.bus.read_byte_data(self.address, Reg.ResultRangeStatus)

        if status & RegResultRangeStatus.BitDeviceReady == 0:
            return None
        status_flags = list()
        if status & RegResultRangeStatus.BitDeviceReady != 0:
            status_flags.append(RegResultRangeStatus.BitDeviceReady)
        for error_value in RegResultRangeStatusError:
            if status & RegResultRangeStatus.MaskError == error_value:
                status_flags.append(error_value)
        print(status_flags, status)

        self.start_range_measurement()

        print('waiting...')
        while True:
            int_status = self.bus.read_byte_data(self.address, Reg.ResultInterruptStatusGpio)
            if (int_status & RegResultInterruptStatusGpio.MaskIntRangeGpio ==
                    RegResultInterruptStatusGpio.IRG_NewSampleReady):
                break
        print('...done')

        range_mm = self.bus.read_byte_data(self.address, Reg.ResultRangeValue)

        self.clear_interrupts()

        return ProximityData(status, status_flags, range_mm)


#        # wait for device to be ready for range measurement
#        while not (self.b.read_byte_data(self._addr, VL6180X_REG_RESULT_RANGE_STATUS) & 0x01): pass
#
#        # Start a range measurement
#        self.b.write_byte_data(self._addr, VL6180X_REG_SYSRANGE_START, 0x01)
#
#        # Poll until bit 2 is set
#        while not (self.b.read_byte_data(self._addr, VL6180X_REG_RESULT_INTERRUPT_STATUS_GPIO) & 0x04): pass
#
#        # read range in mm
#        range = self.b.read_byte_data(self._addr, VL6180X_REG_RESULT_RANGE_VAL)
#
#        # clear interrupt
#        self.b.write_byte_data(self._addr, VL6180X_REG_SYSTEM_INTERRUPT_CLEAR, 0x07)
