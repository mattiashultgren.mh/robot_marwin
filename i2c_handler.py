
import queue
import threading
import time
import math
import sys
sys.path.append('gui')

import smbus
import glm

import pressure
import accel_gyro
import magneto
import proximity
from tuples import ImuData
import led_handler


class I2cHandler(threading.Thread):
    def __init__(self, queues):
        super(I2cHandler, self).__init__(target=self.run, kwargs=self)
        self.i2c_bus = smbus.SMBus(1)
        self.pressure = pressure.Pressure(self.i2c_bus)
        self.accel_gyro = accel_gyro.AccelGyro(self.i2c_bus)
        self.magneto = magneto.Magneto(self.i2c_bus)
        #self.proximity = proximity.Proximity(self.i2c_bus)
        self.queues = queues
        self.in_queue = self.queues['i2c_handler']

        self.avg_p = 0.99
        self.time_step_interval = 0.05

        self.auto_calibration = True

    def run(self):
        self.pressure.setup()
        self.accel_gyro.setup()
        self.magneto.setup()
        #self.proximity.setup()

        vibration_level = 0.0
        vibration_state = 'calm'

        time.sleep(0.1)

        pressure_values = self.pressure.read_values()
        accel_gyro = self.accel_gyro.read_values()
        pressure_avg = pressure_values.pressure
        pressure_slow_avg = pressure_avg

        accel_avg = glm.vec3(accel_gyro.acc_x, accel_gyro.acc_y, accel_gyro.acc_z)
        velo_curr = glm.vec3(0.0, 0.0, 0.0)
        dis_curr = glm.vec3(0.0, 0.0, 0.0)

        gyro_offset = glm.vec3(accel_gyro.gyro_x, accel_gyro.gyro_y, accel_gyro.gyro_z)
        gyro_abs = glm.vec3(0.0, 0.0, 0.0)

        mag_max = glm.vec3(-100.0, -100.0, -100.0)
        mag_min = glm.vec3(100.0, 100.0, 100.0)

        last_start = time.time()
        cntr = 0
        while True:
            time_step = time.time() - last_start
            last_start = time.time()
            try:
                data = self.in_queue.get(timeout=0)
                if type(data) == str:
                    if data == 'calibration=False':
                        self.auto_calibration = False
                        print('Auto_calibration: Off')
                    else:
                        print('Unknown string sent to I2CHandlerThread, ignored!', data)
                else:
                    print('Unknown data sent to I2CHandlerThread, ignored!', data)
            except queue.Empty:
                pass

            cntr += 1
            pressure_values = self.pressure.read_values()
            accel_gyro = self.accel_gyro.read_values()
            magneto_values = self.magneto.read_values()
            #proximity_value = self.proximity.read_values()
            #if proximity_value is not None:
            #    print(proximity_value)

            # print('Magneto - abs**2', magneto_values.mag_x**2 + magneto_values.mag_y**2 + magneto_values.mag_z**2)

            accel_curr = glm.vec3(accel_gyro.acc_x, accel_gyro.acc_y, accel_gyro.acc_z)
            accel_avg = glm.mix(accel_curr, accel_avg, self.avg_p)
            accel_diff = accel_curr - accel_avg

            vibration_state, vibration_level = self.handle_vibration_emotion(glm.length(accel_diff),
                                                                             vibration_state,
                                                                             vibration_level)

            velo_curr = velo_curr * 0.99 + time_step * accel_diff * 9.81

            dis_curr = dis_curr + time_step * velo_curr

            gyro_curr = glm.vec3(accel_gyro.gyro_x, accel_gyro.gyro_y, accel_gyro.gyro_z)
            gyro_offset = glm.mix(gyro_curr, gyro_offset, 0.999)
            gyro_diff = gyro_curr - gyro_offset
            gyro_abs += time_step * (gyro_curr - gyro_offset)
            gyro_abs = (gyro_abs % 360.0) - 180

            mag_curr = glm.vec3(magneto_values.mag_x, magneto_values.mag_y, magneto_values.mag_z)
            if self.auto_calibration:
                mag_max = glm.max(mag_max, mag_curr)
                mag_min = glm.min(mag_min, mag_curr)
            mag_offset = (mag_max + mag_min) / 2.0
            mag_abs = mag_curr - mag_offset
            magnetic_angle = math.degrees(math.atan2(mag_abs.y, mag_abs.x))

            if pressure.RegStatus.P_DA in pressure_values.status:
                p = 0.9
                pressure_avg = pressure_avg * p + (1-p) * pressure_values.pressure
                p = 0.99
                pressure_slow_avg = pressure_slow_avg * p + (1-p) * pressure_values.pressure

            delta_altitude = int((pressure.Pressure.estimate_altitude(pressure_avg) -
                                  pressure.Pressure.estimate_altitude(pressure_slow_avg)) * 100)
            self.queues['controller'].put(ImuData(pressure_avg, delta_altitude,
                                                  accel_diff.x, accel_diff.y, accel_diff.z,
                                                  gyro_diff.x, gyro_diff.y, gyro_diff.z,
                                                  mag_abs.x, mag_abs.y, mag_abs.z))

            if cntr % 20 == 0:
                # print(int(pressure_avg*100)/100, 'hPa |', delta_altitude, 'cm delta altidude')
                self.queues['network'].put(ImuData(pressure_avg, delta_altitude,
                                                   accel_diff.x, accel_diff.y, accel_diff.z,
                                                   #velo_curr.x, velo_curr.y, velo_curr.z,
                                                   #dis_curr.x, dis_curr.y, dis_curr.z,
                                                   #gyro_curr.x, gyro_curr.y, gyro_curr.z,
                                                   gyro_diff.x, gyro_diff.y, gyro_diff.z,
                                                   # gyro_abs.x, gyro_abs.y, gyro_abs.z,
                                                   mag_abs.x, mag_abs.y, mag_abs.z))
#                print('magnetic angle:', magnetic_angle)

            time.sleep(max(0, self.time_step_interval - (time.time() - last_start)))

    def handle_vibration_emotion(self, vibration, state, level):
        if vibration > 0.75:
            level = max(10.0, level)
            print(vibration)
        elif vibration > 0.1:
            level = max(5.0, level)
        else:
            level = max(0.0, level - self.time_step_interval)

        if state == 'calm':
            if level >= 5.0:
                state = 'annoyed'
                self.queues['led'].put(led_handler.LedData(led_handler.LedNames.BottomFront, (0, 255, 0)))
                self.queues['led'].put(led_handler.LedData(led_handler.LedNames.BottomBack, (0, 255, 0)))
        elif state == 'annoyed':
            if level <= 0.0:
                state = 'calm'
                self.queues['led'].put(led_handler.LedData(led_handler.LedNames.BottomFront, (0, 0, 255)))
                self.queues['led'].put(led_handler.LedData(led_handler.LedNames.BottomBack, (0, 0, 255)))
            elif level >= 10.0:
                state = 'angry'
                self.queues['led'].put(led_handler.LedData(led_handler.LedNames.BottomFront, (255, 0, 0)))
                self.queues['led'].put(led_handler.LedData(led_handler.LedNames.BottomBack, (255, 0, 0)))
        elif state == 'angry':
            if level < 5.0:
                state = 'annoyed'
                self.queues['led'].put(led_handler.LedData(led_handler.LedNames.BottomFront, (150, 255, 0)))
                self.queues['led'].put(led_handler.LedData(led_handler.LedNames.BottomBack, (150, 255, 0)))
        return state, level
