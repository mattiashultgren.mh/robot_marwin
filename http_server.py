
import threading
import http.server


http_thread = None


class HttpRequestHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        handlers = {'/camera/png': lambda: http_thread.handle_req_camera('png'),
                    '/camera/jpeg': lambda: http_thread.handle_req_camera('jpeg'),
                    '/camera/vision': http_thread.handle_req_vision}

        f = handlers.get(self.path, None)
        if f is not None:
            content_type, body = f()

            self.send_response(200)

            self.send_header('Content-type', content_type)
            self.end_headers()

            self.wfile.write(body)
        else:
            self.send_response(404)

            self.send_header('Content-type', 'text/html')
            self.end_headers()

            message = 'Could not find...(' + str(self.path) + ')'
            self.wfile.write(bytes(message, "utf8"))


class HttpThread(threading.Thread):
    def __init__(self, queues):
        global http_thread
        super(HttpThread, self).__init__(target=self.run, kwargs=self)
        self.queues = queues
        http_thread = self

    def run(self):
        server_address = ('', 8080)
        httpd = http.server.HTTPServer(server_address, HttpRequestHandler)
        httpd.serve_forever()

    def handle_req_camera(self, file_type):
        self.queues['camera'].put(('req.image.' + file_type, self.queues['http']))
        return 'image/' + file_type, self.queues['http'].get()

    def handle_req_vision(self):
        self.queues['camera'].put(('req.vision.png', self.queues['http']))
        return 'image/png', self.queues['http'].get()
