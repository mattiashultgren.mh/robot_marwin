#!/usr/bin/python3

import sys
import queue
from datetime import datetime
import math

from PyQt5 import QtCore, QtGui, QtWidgets
import matplotlib
matplotlib.use('Qt5Agg')

from ui.RobotMarwinControlCenter import Ui_RobotMarwinControlCenter
from tuples import ConnectionParameters, MotorControlData, ImuData
from network_thread import NetworkThread


class RobotMarwinControlCenter(QtWidgets.QMainWindow, Ui_RobotMarwinControlCenter):
    def __init__(self):
        super(RobotMarwinControlCenter, self).__init__()

        # Setup the UI from the mockup
        self.setupUi(self)

        # connect ui signals
        self.lineEditIpAddress.textChanged.connect(self.signal_connection_parameter_changed)
        self.spinBoxPort.valueChanged.connect(self.signal_connection_parameter_changed)
        self.pushButtonMotorForward.clicked.connect(lambda x: self.signal_button_motor(1, 1))
        self.pushButtonMotorBackward.clicked.connect(lambda x: self.signal_button_motor(-1, -1))
        self.pushButtonMotorStop.clicked.connect(lambda x: self.signal_button_motor(0, 0))
        self.pushButtonMotorTurnLeft.clicked.connect(lambda x: self.signal_button_motor(-1, 1))
        self.pushButtonMotorTurnRight.clicked.connect(lambda x: self.signal_button_motor(1, -1))

        # setup network thread and connect signals
        self.queue_network = queue.Queue()
        self.thread_network = NetworkThread(in_queue=self.queue_network)
        self.thread_network.sig_log_message.connect(self.append_text_with)
        self.thread_network.sig_motor_encoders.connect(self.update_motor_encoder_info)
        self.thread_network.sig_battery_voltage.connect(self.update_battery_voltage)
        self.thread_network.sig_imu_data.connect(self.update_imu_info)

        self.plot_battery = self.widgetPlot.canvas.fig.add_subplot(411)
        self.plot_accel = self.widgetPlot.canvas.fig.add_subplot(412)
        self.plot_gyro = self.widgetPlot.canvas.fig.add_subplot(413)
        self.plot_mag = self.widgetPlot.canvas.fig.add_subplot(414)

        self.battery_data = [list(), list()]
        self.data_accel = [list(), list(), list(), list()]
        self.data_gyro = [list(), list(), list(), list()]
        self.data_mag = [list(), list(), list(), list()]
        self.show_plots()
        self.motor_integral = 0.0

    def show(self):
        super(RobotMarwinControlCenter, self).show()
        self.thread_network.start()
        self.signal_connection_parameter_changed()

    def signal_connection_parameter_changed(self, **_):
        ip_addr = self.lineEditIpAddress.text()
        port = self.spinBoxPort.value()
        self.queue_network.put(ConnectionParameters(ip_addr, port))

    def append_text_with(self, string):
        self.textInfo.setText(self.textInfo.toPlainText() + string + '\n')
        self.textInfo.moveCursor(QtGui.QTextCursor.End)

    def update_motor_encoder_info(self, m1, m2, m3, m4):
        self.labelFrontLeft.setText(str(m1))
        self.labelFrontRight.setText(str(m2))
        self.labelBackLeft.setText(str(m3))
        self.labelBackRight.setText(str(m4))

    def update_battery_voltage(self, voltage):
        self.labelBatteryVoltage.setText(f'{voltage} mV')
        self.battery_data[0].append(datetime.now())
        self.battery_data[1].append(voltage)
        self.plot_battery.plot(*self.battery_data, 'r')
        self.widgetPlot.canvas.draw()

    def append_multi_axis_point(self, dest, point):
        dest[0].append(datetime.now())
        for i in range(len(point)):
            dest[1+i].append(point[i])
        for i in range(len(dest)):
            dest[i] = dest[i][-60:]

    def show_plots(self):
        self.plot_accel.cla()
        self.plot_gyro.cla()
        self.plot_mag.cla()
        self.plot_battery.set_title('Battery voltage')
        self.plot_accel.set_title('Acceleration')
        self.plot_gyro.set_title('Gyroscope')
        self.plot_mag.set_title('Magnetometer')
        self.plot_battery.set_ylabel('mV')
        self.plot_accel.set_ylabel('g')
        self.plot_gyro.set_ylabel('degree/s')
        self.plot_mag.set_ylabel('gauss')
        for i in range(3):
            self.plot_accel.plot(self.data_accel[0], self.data_accel[1+i], 'rgb'[i])
            self.plot_gyro.plot(self.data_gyro[0], self.data_gyro[1+i], 'rgb'[i])
            self.plot_mag.plot(self.data_mag[0], self.data_mag[1 + i], 'rgb'[i])
        self.widgetPlot.canvas.draw()

    def update_imu_info(self, imu_data):
        self.labelPressureInfo.setText(f'{imu_data.pressure:.5} hPa   {imu_data.delta_altitude} cm')
        self.labelAccelInfo.setText(f'Raw({float(imu_data.accel_x):.3}, {float(imu_data.accel_y):.3}, {float(imu_data.accel_z):.3})')
        self.labelGyroInfo.setText(f'Raw({float(imu_data.gyro_x):.3}, {float(imu_data.gyro_y):.3}, {float(imu_data.gyro_z):.3})')
        self.labelMagInfo.setText(f'Raw({float(imu_data.mag_x):.3}, {float(imu_data.mag_y):.3}, {float(imu_data.mag_z):.3})')

        self.append_multi_axis_point(self.data_accel, (imu_data.accel_x, imu_data.accel_y, imu_data.accel_z))
        self.append_multi_axis_point(self.data_gyro, (imu_data.gyro_x, imu_data.gyro_y, imu_data.gyro_z))
        self.append_multi_axis_point(self.data_mag, (imu_data.mag_x, imu_data.mag_y, imu_data.mag_z))
        self.show_plots()
        if self.checkBoxAutoNorth.checkState() == QtCore.Qt.Checked:
            magnetic_angle = math.degrees(math.atan2(imu_data.mag_y, imu_data.mag_x))
            print(magnetic_angle, self.motor_integral)
            magnetic_angle /= 180.0

            self.motor_integral += magnetic_angle * 0.5
            if self.motor_integral * magnetic_angle < 0.0:
                self.motor_integral *= 0.1
            speed = int(255.0 * (magnetic_angle + self.motor_integral))
            if abs(magnetic_angle) < 5.0/180.0:
                speed = 0
            speed = max(-255, min(255, speed))
            self.send_motor_control(-1, 1, speed)

    def signal_button_motor(self, left, right):
        self.send_motor_control(left, right, self.horizontalSliderSpeed.value())

    def send_motor_control(self, left, right, speed):
        left_speed = abs(speed * left)
        right_speed = abs(speed * right)

        front_left_dir = 0 if (speed*left) >= 0 else 1
        front_right_dir = 0 if (speed*right) >= 0 else 1

        back_left_dir = front_left_dir ^ 1
        back_right_dir = front_right_dir ^ 1

        self.queue_network.put(MotorControlData([left_speed, right_speed, left_speed, right_speed],
                                                (front_left_dir, front_right_dir,
                                                 back_left_dir, back_right_dir)))


def start_app():
    app = QtWidgets.QApplication(sys.argv)
    main_window = RobotMarwinControlCenter()
    main_window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    start_app()

