
import queue
import socket
import pickle
import sys
sys.path.append('..')

from PyQt5 import QtCore

from tuples import ConnectionParameters, MotorControlData, ImuData
from PacketHandler import ReportBattery, ReportEncoder


class NetworkThread(QtCore.QThread):
    sig_log_message = QtCore.pyqtSignal(str)
    sig_motor_encoders = QtCore.pyqtSignal(int, int, int, int)
    sig_battery_voltage = QtCore.pyqtSignal(int)
    sig_imu_data = QtCore.pyqtSignal(ImuData)

    def __init__(self, in_queue, parent=None):
        QtCore.QThread.__init__(self, parent)
        self.in_queue = in_queue
        self.socket = None
        self.con_param = None

    def run(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.settimeout(0.1)
        self.socket.bind(('', 4455))

        while True:
            try:
                data = self.in_queue.get(block=False)
                if type(data) == ConnectionParameters:
                    self.sig_log_message.emit(f'Got connection parameters, {data}')
                    self.con_param = data

                    self.send('hello')
                else:
                    self.send(data)
            except queue.Empty:
                pass

            if self.socket is not None:
                try:
                    raw_data, server = self.socket.recvfrom(1024)
                    data = NetworkThread.parse_received_data(raw_data)

                    if type(data) == ReportBattery:
                        self.sig_battery_voltage.emit(data.BatteryVoltage)
                    elif type(data) == ReportEncoder:
                        self.sig_motor_encoders.emit(data.Motor0, data.Motor1, data.Motor2, data.Motor3)
                    elif type(data) == ImuData:
                        self.sig_imu_data.emit(data)
                    else:
                        self.sig_log_message.emit(f'Got {len(raw_data)} bytes containing "{data}" from {server}')

                except socket.timeout:
                    pass

    @staticmethod
    def parse_received_data(raw_data):
        if len(raw_data) > 4:
            data_size = int.from_bytes(raw_data[:4], byteorder='little')
            if data_size == len(raw_data[4:]):
                return pickle.loads(raw_data[4:])
        return None

    def send(self, data):
        if self.socket is not None:
            try:
                data_to_send = pickle.dumps(data, protocol=4)
                data_to_send = len(data_to_send).to_bytes(4, byteorder='little') + data_to_send
                self.socket.sendto(data_to_send, self.con_param)
                self.sig_log_message.emit(f'Sent {len(data_to_send)} bytes to {self.con_param}')
            except Exception as ex:
                self.sig_log_message.emit(f'Exception during udp send, {ex}')


