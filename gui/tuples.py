
import collections


ConnectionParameters = collections.namedtuple('ConnectionParameters', 'ip_addr port')


MotorControlData = collections.namedtuple('MotorControlData', 'pwms dirs')
WeaponControlData = collections.namedtuple('WeaponControlData', 'pwm dir')

ImuData = collections.namedtuple('ImuData', 'pressure delta_altitude '
                                            'accel_x accel_y accel_z '
                                            'gyro_x gyro_y gyro_z '
                                            'mag_x mag_y mag_z')

VisionInformation = collections.namedtuple('VisionInformation', 'floor opponent')
