
import queue
import threading
import io
import time
import math
import sys

import picamera
import picamera.array
import cv2
import numpy as np

sys.path.append('gui')

import tuples



class CameraHandler(threading.Thread):
    def __init__(self, queues):
        super(CameraHandler, self).__init__(target=self.run, kwargs=self)
        self.queues = queues
        self.camera = None

    @staticmethod
    def calc_distance(x, y):
        y = 480 - y
        if y < 100:
            y_part = (20 / 65) * (y / 100)
        else:
            y_part = (20 / 65) + (45 / 65) * ((y - 100) / 60)
        xcm = max((15 + 30 * y_part) * abs(x - 320) / 320 - 10, 0)
        ycm = 30 + 65 * y_part
        dist = math.sqrt(xcm ** 2 + ycm ** 2)
        return dist

    def mark_floor_and_opponent(self, mask, image):
        floor_distance = list()
        mean_distance = 0
        for x in range(8, len(mask[0]), 16):
            for y in range(len(mask) - 1, 320, -1):
                if mask[y][x][0] == 0:
                    break
            floor_distance.append((x, y, self.calc_distance(x, y)))
            mean_distance += floor_distance[-1][-1]
        mean_distance *= 1 / len(floor_distance)

        opponent_detected = False
        opponent_distance = 0
        opponent_distances = 0
        for x, y, d in floor_distance:
            if d > 32 and d < mean_distance * 0.9 and abs(x-320) < 160:
                color = (0, 0, 255)
                opponent_detected = True
                opponent_distance += d
                opponent_distances += 1
            else:
                color = (0, 255, 0)
            image[y][x] = color
            image[y - 1][x] = color
            image[y - 2][x] = color
            image[y][x + 1] = color
            image[y - 1][x + 1] = color
            image[y - 2][x + 1] = color
            image[y][x - 1] = color
            image[y - 1][x - 1] = color
            image[y - 2][x - 1] = color
        if opponent_distances != 0:
            opponent_distance /= opponent_distances
        return (mean_distance > 32, mean_distance), (opponent_detected, opponent_distance)

    def vision(self, raw_capture):
        start = time.time()
        self.camera.capture(raw_capture, format='bgr', use_video_port=True)
        #print('capture time:', time.time() - start)

        image = np.float32(raw_capture.array)
        raw_capture.truncate(0)

        img_size = image.shape
        # print(img_size)

        hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
        gray_image = np.uint8(cv2.cvtColor(image, cv2.COLOR_BGR2GRAY))
        hue_max = hsv_image[:, :, 0].max()
        sat_max = hsv_image[:, :, 1].max()
        sat_mean = hsv_image[:, :, 1].mean()
        val_max = hsv_image[:, :, 2].max()
        #print(hue_max, sat_mean, sat_max, val_max)
        hue = hsv_image[:, :, 0]
        saturation = hsv_image[:, :, 1]
        value = hsv_image[:, :, 2]
        edge = cv2.Canny(gray_image, 100, 200)

        hue *= 255 / hue.max()
        saturation *= 255 / saturation.max()
        value *= 255 / value.max()

        mask1 = cv2.inRange(hsv_image,
                            (0, 0, 0),
                            (255, 255 * (1 + sat_mean) / 2, 255))
        mask2 = cv2.inRange(hsv_image,
                            (0, 0, 50),
                            (255, 255, 255))
        mask3 = cv2.inRange(edge, (0,), (60,))

        mask1 = cv2.erode(mask1, None, iterations=3)
        mask2 = cv2.erode(mask2, None, iterations=3)
        mask3 = cv2.erode(mask3, None, iterations=3)

        comb_mask = cv2.bitwise_and(mask1, mask2, mask=mask3)

        mask = np.zeros((img_size[0], img_size[1], 1), np.uint8)

        contours, hierarchy = cv2.findContours(comb_mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
        # print(contours)
        # print(heir)
        for i, c in enumerate(contours):
            max_y = max(x[0][1] for x in c)
            if max_y == img_size[0] - 1:
                cv2.drawContours(mask, contours, i, (255,), -1, hierarchy=hierarchy)

        masked_image = cv2.bitwise_and(image, image, mask=mask)

        floor_dist, opponent = self.mark_floor_and_opponent(masked_image, image)

#        out_image = np.zeros((img_size[0] * 4, img_size[1] * 2, 3), np.uint8)
#        out_image[0 * img_size[0]:1 * img_size[0], 0 * img_size[1]:1 * img_size[1]] = image
#        out_image[0 * img_size[0]:1 * img_size[0], 1 * img_size[1]:2 * img_size[1]] = masked_image
#        for ch in range(3):
#            out_image[1 * img_size[0]:2 * img_size[0], 0 * img_size[1]:1 * img_size[1], ch] = saturation
#            out_image[1 * img_size[0]:2 * img_size[0], 1 * img_size[1]:2 * img_size[1], ch] = value
#            out_image[2 * img_size[0]:3 * img_size[0], 0 * img_size[1]:1 * img_size[1], ch] = mask1
#            out_image[2 * img_size[0]:3 * img_size[0], 1 * img_size[1]:2 * img_size[1], ch] = mask2
#            out_image[3 * img_size[0]:4 * img_size[0], 0 * img_size[1]:1 * img_size[1], ch] = mask3
#            out_image[3 * img_size[0]:4 * img_size[0], 1 * img_size[1]:2 * img_size[1], ch] = comb_mask

        #print('vision time:', time.time() - start)
        return image, floor_dist, opponent

    def run(self):
        self.camera = picamera.PiCamera(resolution=(640, 480))
        raw_capture = picamera.array.PiRGBArray(self.camera)
        while True:
            try:
                req_type, return_queue = self.queues['camera'].get(timeout=0.05)
                if req_type == 'req.vision.png':
                    image, floor_distance, opponent = self.vision(raw_capture)
                    print(floor_distance, opponent)

                    buf = cv2.imencode('.png', image)[1]
                    return_queue.put(buf)
                elif req_type.startswith('req.image.'):
                    byte_stream = io.BytesIO()
                    start = time.time()
                    self.camera.capture(byte_stream,
                                        format=req_type.split('.')[-1],
                                        use_video_port=True)
                    byte_stream.seek(0)
                    data = byte_stream.read()
                    print('capture time:', time.time() - start)
                    return_queue.put(data)
            except queue.Empty:
                image, floor, opponent = self.vision(raw_capture)
                #print(floor, opponent)
                self.queues['controller'].put(tuples.VisionInformation(floor, opponent))

