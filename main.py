
import time
import queue
import sys

sys.path.append('gui')

import serial

from PacketHandler import Serializer, Deserializer, PacketType
from network_thread import NetworkThread
from tuples import MotorControlData, WeaponControlData
from i2c_handler import I2cHandler
import led_handler
import http_server
import camera
import controller


def serial_port_handler(queues):
    port = serial.Serial('/dev/ttyS0', baudrate=115200)

    parser = Deserializer()
    last_encoder = None

    while True:
        data = port.read()
        if parser.add_byte(data):
            pkt = parser.get_packet()
            if pkt is not None:
                if pkt.PacketType == PacketType.ReportBattery:
                    queues['network'].put(pkt)
                    queues['controller'].put(pkt)
                elif pkt.PacketType == PacketType.ReportDistanceVoltage:
                    queues['network'].put(pkt)
                    queues['controller'].put(pkt)
                elif pkt.PacketType == PacketType.ReportEncoder:
                    if (last_encoder is None or
                            pkt.Motor0 != last_encoder.Motor0 or
                            pkt.Motor1 != last_encoder.Motor1 or
                            pkt.Motor2 != last_encoder.Motor2 or
                            pkt.Motor3 != last_encoder.Motor3):
                        last_encoder = pkt
                        queues['network'].put(pkt)
                        queues['controller'].put(pkt)
                else:
                    print("Generic parsed as", pkt)
            else:
                print('Correct packet received', parser.data, 'but cold not parse it')

            if pkt.SeqNum == 157:
                tmp_data = Serializer.read_battery()
                print("Sending", tmp_data)
                port.write(tmp_data)
#            elif pkt.SeqNum == 35:
#                print('Weapon direction 0')
#                tmp_data = Serializer.set_weapon_speed(255, 0)
#                port.write(tmp_data)
#            elif pkt.SeqNum == 70:
#                print('Weapon direction 1')
#                tmp_data = Serializer.set_weapon_speed(255, 1)
#                port.write(tmp_data)
#            elif pkt.SeqNum == 100:
#                print('Weapon off')
#                tmp_data = Serializer.set_weapon_speed(0, 0)
#                port.write(tmp_data)
            parser.clear()
        try:
            data = queues['serial'].get(block=False)
            if type(data) == MotorControlData:
                tmp_data = Serializer.set_motor_speed(data.pwms, data.dirs)
                print("Sending", tmp_data)
                port.write(tmp_data)
            elif type(data) == WeaponControlData:
                tmp_data = Serializer.set_weapon_speed(data.pwm, data.dir)
                print("Sending", tmp_data)
                port.write(tmp_data)
            else:
                print('Serial got', data, 'in the queue')
        except queue.Empty:
            pass


def start_marwin():
    queues = {'network': queue.Queue(),
              'serial': queue.Queue(),
              'led': queue.Queue(),
              'camera': queue.Queue(),
              'http': queue.Queue(),
              'controller': queue.Queue(),
              'i2c_handler': queue.Queue()}

    threads = list()
    threads.append(NetworkThread(queues))
    threads.append(I2cHandler(queues))
    threads.append(led_handler.LedThread(queues))
    threads.append(http_server.HttpThread(queues))
    threads.append(camera.CameraHandler(queues))
    threads.append(controller.Controller(queues))

    for t in threads:
        t.start()

    serial_port_handler(queues)


if __name__ == '__main__':
    start_marwin()


