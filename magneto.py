
from enum import IntEnum
import collections

import smbus

MagnetoRawData = collections.namedtuple('MagnetoRawData', 'status mag_x mag_y mag_z')
MagnetoData = collections.namedtuple('MagnetoData', 'status mag_x mag_y mag_z')


class Reg(IntEnum):
    AutoIncrement = 0x80
    WhoAmI = 0x0f

    Ctrl1 = 0x20
    Ctrl2 = 0x21
    Ctrl3 = 0x22
    Ctrl4 = 0x23
    Ctrl5 = 0x24
    Status = 0x27


class RegWhoAmI(IntEnum):
    Value = 0x3D


class RegCtrl1(IntEnum):
    MaskOM = 0x60  # OperationMode
    OM_LowPower = 0x00
    OM_MediumPerformance = 0x20
    OM_HighPerformance = 0x40
    OM_UltraHighPerformance = 0x60
    MaskDO = 0x1C
    DO_0_625Hz = 0x00
    DO_1_25Hz = 0x04
    DO_2_5Hz = 0x08
    DO_5Hz = 0x0C
    DO_10Hz = 0x10
    DO_20Hz = 0x14
    DO_40Hz = 0x18
    DO_80Hz = 0x1C
    BitFastODR = 0x02
    BitSelfTest = 0x01


class RegCtrl2(IntEnum):
    BitSoftReset = 0x04
    BitReboot = 0x08
    MaskFullScale = 0x60
    FullScale_4gauss = 0x00
    FullScale_8gauss = 0x01
    FullScale_12gauss = 0x02
    FullScale_16gauss = 0x03


class RegCtrl3(IntEnum):
    BitLowPower= 0x20
    BitSpiInterfaceMode = 0x04
    MaskMode = 0x03
    Mode_Continuous = 0x00
    Mode_SingleConversion = 0x01
    Mode_PowerDown = 0x02
    Mode_PowerDownDefault = 0x03


class RegCtrl5(IntEnum):
    BitFastRead = 0x80
    BitBlockDataUpdate = 0x40


class RegStatus(IntEnum):
    ZYX_OR = 0x80
    Z_OR = 0x40
    Y_OR = 0x20
    X_OR = 0x10
    ZYX_DA = 0x08
    Z_DA = 0x04
    Y_DA = 0x02
    X_DA = 0x01


class Magneto:
    def __init__(self, bus, address=0x1E):
        self.chip_name = 'LIS3MDL'
        self.address = address
        self.bus = bus
        self.full_scale_gauss = None

    def setup(self, odr=RegCtrl1.DO_10Hz):
        id = self.bus.read_byte_data(self.address, Reg.WhoAmI)
        if id != RegWhoAmI.Value:
            print('Failed to read id register, expected', RegWhoAmI.Value, 'but got', id)

        # Enable the specified DO and ultra high performance
        reg_value = (RegCtrl1.OM_UltraHighPerformance | odr)
        self.bus.write_byte_data(self.address, Reg.Ctrl1, reg_value)

        # Set the full scale output
        reg_value = RegCtrl2.FullScale_4gauss
        self.full_scale_gauss = 4
        self.bus.write_byte_data(self.address, Reg.Ctrl2, reg_value)

        # Enable continuous mode
        reg_value = RegCtrl3.Mode_Continuous
        self.bus.write_byte_data(self.address, Reg.Ctrl3, reg_value)

        # Enable block data update
        reg_value = RegCtrl5.BitBlockDataUpdate
        self.bus.write_byte_data(self.address, Reg.Ctrl5, reg_value)

    def read_values(self):
        data = self.bus.read_i2c_block_data(self.address,
                                            Reg.AutoIncrement + Reg.Status, 7)
        if len(data) != 7:
            print('Size of data read is not matching the expected format')
            return None
        inter_raw_data = MagnetoRawData(data[0],
                                        int.from_bytes(data[1:3], byteorder='little', signed=True),
                                        int.from_bytes(data[3:5], byteorder='little', signed=True),
                                        int.from_bytes(data[5:], byteorder='little', signed=True))
        status_flags = list()
        for bit in RegStatus:
            if inter_raw_data.status & bit != 0:
                status_flags.append(bit)
        inter_data = MagnetoData(tuple(status_flags),
                                 (inter_raw_data.mag_x / 2**15) * self.full_scale_gauss,
                                 (inter_raw_data.mag_y / 2**15) * self.full_scale_gauss,
                                 (inter_raw_data.mag_z / 2**15) * self.full_scale_gauss)
        return inter_data



