
import queue
import socket
import pickle
import threading
import sys

sys.path.append('gui')

from tuples import MotorControlData


class NetworkThread(threading.Thread):
    def __init__(self, queues):
        super(NetworkThread, self).__init__(target=self.run, kwargs=self)
        self.queues = queues
        self.in_queue = queues['network']
        self.serial_queue = queues['serial']
        self.socket = None
        self.con_param = None

    def run(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.settimeout(0.1)
        self.socket.bind(('', 4455))

        while True:
            try:
                data = self.in_queue.get(block=False)
                self.send(data)
            except queue.Empty:
                pass

            if self.socket is not None:
                try:
                    raw_data, con_param = self.socket.recvfrom(1024)
                    data = NetworkThread.parse_received_data(raw_data)
                    self.con_param = con_param
                    if type(data) == MotorControlData:
                        print(data)
                        self.serial_queue.put(data)
                    else:
                        print('Got', len(raw_data), 'bytes containing', data, 'from', con_param)
                except socket.timeout:
                    pass

    @staticmethod
    def parse_received_data(raw_data):
        if len(raw_data) > 4:
            data_size = int.from_bytes(raw_data[:4], byteorder='little')
            if data_size == len(raw_data[4:]):
                return pickle.loads(raw_data[4:])
        return None

    def send(self, data):
        if self.socket is not None and self.con_param is not None:
            try:
                data_to_send = pickle.dumps(data, protocol=4)
                data_to_send = len(data_to_send).to_bytes(4, byteorder='little') + data_to_send
                self.socket.sendto(data_to_send, self.con_param)
                print('Sent', len(data_to_send), 'bytes to', self.con_param)
            except Exception as ex:
                print('Exception during udp send,', ex)


