from enum import IntEnum
import collections

import smbus


AccelGyroRawData = collections.namedtuple('AccelGyroRawData', 'status gyro_x gyro_y gyro_z '
                                                              'acc_x acc_y acc_z')
AccelGyroData = collections.namedtuple('AccelGyroData', 'status gyro_x gyro_y gyro_z '
                                                        'acc_x acc_y acc_z')


class Reg(IntEnum):
    WhoAmI = 0x0f

    Ctrl1_Accel = 0x10
    Ctrl2_Gyro = 0x11

    Ctrl9_Accel = 0x18
    Ctrl10_Gyro = 0x19

    Status = 0x1E

    TempOutL = 0x20
    TempOutH = 0x21
    GyroOutXL = 0x22
    GyroOutXH = 0x23
    GyroOutYL = 0x24
    GyroOutYH = 0x25
    GyroOutZL = 0x26
    GyroOutZH = 0x27
    AccelOutXL = 0x28
    AccelOutXH = 0x29
    AccelOutYL = 0x2A
    AccelOutYH = 0x2B
    AccelOutZL = 0x2C
    AccelOutZH = 0x2D


class RegWhoAmI(IntEnum):
    Value = 0x69


class RegCtrl1_Accel(IntEnum):
    MaskOdr = 0xF0
    Odr_PowerDown = 0x00
    Odr_13Hz = 0x10
    Odr_26Hz = 0x20
    Odr_52Hz = 0x30
    Odr_104Hz = 0x40
    Odr_208Hz = 0x50
    Odr_416Hz = 0x60
    Odr_833Hz = 0x70
    Odr_1_66kHz = 0x80
    Odr_3_33kHz = 0x90
    Odr_6_66kHz = 0xA0

    MaskFullScale = 0x0C
    FullScale_2g = 0x00
    FullScale_4g = 0x08
    FullScale_8g = 0x0C
    FullScale_16g = 0x04

    MaskAntiAliasingBandWidth = 0x03
    AntiAliasingBandWidth_400Hz = 0x00
    AntiAliasingBandWidth_200Hz = 0x01
    AntiAliasingBandWidth_100Hz = 0x02
    AntiAliasingBandWidth_50Hz = 0x03


class RegCtrl2_Gyro(IntEnum):
    MaskOdr = 0xF0
    Odr_PowerDown = 0x00
    Odr_13Hz = 0x10
    Odr_26Hz = 0x20
    Odr_52Hz = 0x30
    Odr_104Hz = 0x40
    Odr_208Hz = 0x50
    Odr_416Hz = 0x60
    Odr_833Hz = 0x70
    Odr_1_66kHz = 0x80

    MaskFullScale = 0x0C
    FullScale_245dps = 0x00
    FullScale_500dps = 0x04
    FullScale_1000dps = 0x08
    FullScale_2000dps = 0x0C
    BitFullScale_125dps = 0x02


class RegCtrl9_Accel(IntEnum):
    BitXen = 0x08
    BitYen = 0x10
    BitZen = 0x20


class RegCtrl10_Gyro(IntEnum):
    BitXen = 0x08
    BitYen = 0x10
    BitZen = 0x20
    BitEmbFunctions = 0x04
    BitPedometerReset = 0x02
    BitSignificantMotion = 0x01


class AccelGyro:
    def __init__(self, bus, address=0x6b):
        self.chip_name = 'LSM6DS33'
        self.address = address
        self.bus = bus
        self.full_scale_accel = None
        self.full_scale_gyro = None

    def setup(self, odr_accel=RegCtrl1_Accel.Odr_13Hz, odr_gyro=RegCtrl2_Gyro.Odr_13Hz):
        id = self.bus.read_byte_data(self.address, Reg.WhoAmI)
        if id != RegWhoAmI.Value:
            print('Failed to read id register, expected', RegWhoAmI.Value, 'but got', id)

        # Enable accelerometer axis sampling
        reg_value = (RegCtrl9_Accel.BitXen |
                     RegCtrl9_Accel.BitYen |
                     RegCtrl9_Accel.BitZen)
        self.bus.write_byte_data(self.address, Reg.Ctrl9_Accel, reg_value)

        # Set the ODR, FullScale, and anti-aliasing bandwidth for the accelerometer
        reg_value = (odr_accel |
                     RegCtrl1_Accel.AntiAliasingBandWidth_400Hz |
                     RegCtrl1_Accel.FullScale_2g)
        self.bus.write_byte_data(self.address, Reg.Ctrl1_Accel, reg_value)
        self.full_scale_accel = 2

        # Enable gyro axis sampling
        reg_value = (RegCtrl10_Gyro.BitXen |
                     RegCtrl10_Gyro.BitYen |
                     RegCtrl10_Gyro.BitZen)
        self.bus.write_byte_data(self.address, Reg.Ctrl10_Gyro, reg_value)

        # Set the ODR, and FullScale for the gyro
        reg_value = (odr_gyro |
                     RegCtrl2_Gyro.FullScale_1000dps)
        self.bus.write_byte_data(self.address, Reg.Ctrl2_Gyro, reg_value)
        self.full_scale_gyro = 1000

    def read_values(self):
        data = self.bus.read_i2c_block_data(self.address, Reg.Status, 16)
        if len(data) != 16:
            print('Size of data read is not matching the expected format')
            return None
        inter_raw_data = AccelGyroRawData(data[0],
                                          int.from_bytes(data[4:6], byteorder='little', signed=True),
                                          int.from_bytes(data[6:8], byteorder='little', signed=True),
                                          int.from_bytes(data[8:10], byteorder='little', signed=True),
                                          int.from_bytes(data[10:12], byteorder='little', signed=True),
                                          int.from_bytes(data[12:14], byteorder='little', signed=True),
                                          int.from_bytes(data[14:16], byteorder='little', signed=True))
        inter_data = AccelGyroData(data[0],
                                   (inter_raw_data.gyro_x / 2**15) * self.full_scale_gyro,
                                   (inter_raw_data.gyro_y / 2**15) * self.full_scale_gyro,
                                   (inter_raw_data.gyro_z / 2**15) * self.full_scale_gyro,
                                   (inter_raw_data.acc_x / 2**15) * self.full_scale_accel,
                                   (inter_raw_data.acc_y / 2**15) * self.full_scale_accel,
                                   (inter_raw_data.acc_z / 2**15) * self.full_scale_accel)
        return inter_data



