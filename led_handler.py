
import time
import threading
import random
from enum import IntEnum
import collections
import queue

import board
import neopixel


LedData = collections.namedtuple('LedData', 'name value')

class LedNames(IntEnum):
    BottomFront = 0
    BottomBack = 1


class Led:
    def __init__(self):
        self.color = [0, 0, 255]
        self.base_color = [0, 0, 255]
        self.aim_color = None
        self.aim_time = None
        self.last_time_update = None
        self.aim_time_delta = 0.5
        self.aim_delta = 50

    def get_color(self):
        return tuple(int(x) for x in self.color)

    def animate_color(self, clock_tick):
        if self.aim_color is None:
            self.aim_color = [random.randrange(max(0, x-self.aim_delta), min(255, x+self.aim_delta))
                              for x in self.base_color]
            self.aim_time = clock_tick + self.aim_time_delta
            self.last_time_update = clock_tick

        percentage = min(1.0,
                         (clock_tick - self.last_time_update) / (self.aim_time - self.last_time_update))
        for idx in range(len(self.color)):
            diff = (self.aim_color[idx] - self.color[idx]) * percentage
            self.color[idx] += diff

        if clock_tick >= self.aim_time:
            self.aim_color = None
            self.aim_time = None
            self.last_time_update = None
        else:
            self.last_time_update = clock_tick


class LedThread(threading.Thread):
    def __init__(self, queues, pin=board.D18, pixel_order=neopixel.RGB):
        super(LedThread, self).__init__(target=self.run, kwargs=self)
        self.queues = queues
        self.in_queue = queues['led']
        self.pin = pin
        self.pixel_order = pixel_order

        self.leds = list()
        for _ in range(2):
            self.leds.append(Led())
        self.pixels = None

    def run(self):
        try:
            self.pixels = neopixel.NeoPixel(self.pin, len(self.leds),
                                            pixel_order=self.pixel_order,
                                            auto_write=False)
        except RuntimeError as rte:
            print(rte, 'thus we skip the led controller')
            return

        while True:
            self.update_pixels()
            try:
                data = self.in_queue.get(timeout=0.05)
                if type(data) == LedData:
                    self.leds[data.name].base_color = data.value
                else:
                    print('Unknown data sent to LedThread, ignored!')
            except queue.Empty:
                pass

    def update_pixels(self):
        clock_tick = time.time()
        for idx, led in enumerate(self.leds):
            led.animate_color(clock_tick)
            self.pixels[idx] = led.get_color()
        self.pixels.show()

