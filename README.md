# Connetion details

Robot Marwin will connect to the nearby wifi network called "flammande".

It has the ip 192.168.0.104.
SSH is enabled and can be connected with the user:pi, password:marwinpi.

# Pinning
## Arduino

|Name |Function|Pin|
|-----|--------|---|
| Ch1 |  PWM   | 3 |
| Ch1 |  DIR   | 2 |
| Ch1 |  QE A  | 12|
| Ch1 |  QE B  | 13|
|-----|--------|---|
| Ch2 |  PWM   | 5 |
| Ch2 |  DIR   | 4 |
| Ch2 |  QE A  | A4|
| Ch2 |  QE B  | A5|
|-----|--------|---|
| Ch3 |  PWM   | 6 |
| Ch3 |  DIR   | 7 |
| Ch3 |  QE A  | 10|
| Ch3 |  QE B  | A1|
|-----|--------|---|
| Ch4 |  PWM   | 9 |
| Ch4 |  DIR   | 8 |
| Ch4 |  QE A  | A2|
| Ch4 |  QE B  | A3|
|-----|--------|---|
| Weapon | PWM | 11|
| Weapon | DIR | A0|
|-----|--------|---|
| Vbat|  Meas  | A6|
| Distance|Meas| A7|
|-----|--------|---|
| RX  | RPi com| RX|
| TX  | RPi com| TX|
|-----|--------|---|
| 11  | Program| 11|
| 12  | Program| 12|
| 13  | Program| 13|
| 10  | Program|RST|


## Raspberry Pi

|Name |Function|Pin|
|-----|--------|---|
| RX  | Ard com|RXD|
| TX  | Ard com|TXD|
|-----|--------|---|
| SDA | IMU com|SDA|
| SCL | IMU com|SCL|
|-----|--------|---|
| LED-Ring | PWM0   | 18|
| LED-Bottom | PWM0   | 12|

# Serial protocol

All values are coded as LE.

|   Name   | Size| Comment                             |
|----------|:---:|-------------------------------------|
|Start byte|  1  | 0xC4                                |
|  Length  |  1  | Size in bytes, including start byte |
| Seq.Num. |  1  | Sequence number, RX and TX will ave their own sequence. |
|  MsgType |  1  | Message type                        |
|  MsgData |  X  | Size will be (Length-6) bytes.      |
|  ChkSum  |  2  | ChkSum, see below.                  |

## CheckSum

The checksum is computed by initializing a 16 bit register with 0xFFFF.
Then for every byte one multiply the register by 257 and add the byte to register.
After adding all the bytes this way, the value of the register is the checksum.

## Message type

| MsgType |     Name     | MsgDataSize | Comment                        |
|:-------:|--------------|:-----------:|--------------------------------|
|  0x00   | ReadBattery  |     NA      | Request of battery reading.    |
|  0x01   | ReportBattery|     2       | Battery level in mV.           |
|  0x02   | ReportEncoder|    4x4      | The four motor encoder values. |
|  0x03   | SetMotorSpeed|  4x1 + 4x1  | First 4 motor pwm values followed by four motor direction registers. |
|  0x04   | SetWeaponSpeed|   1 + 1    | Weapon pwm value followed by direction register. |
|  0x05   | ReportDistanceVoltage|  2  | Voltage over the distance sensor. |

# Package and software dependencies

Note that this section was not started from the beginning of the project thus it is very much lacking and is not a full list.

## Arduino
The Arduino studio with standard libraries that comes with the studio is needed, nothing on top of that

## Raspberry
Based on the latest raspbian stretch lite.
Enabled the SSH by placing a file called ssh in the boot at the sd card.
Use `sudo raspi-config` to enable the serial ports and the camera, and also to setup the wifi auto connect parameters.

### Python modules
```bash
sudo python3 -m pip numpy
sudo python3 -m pip pyglm

```


### Neopixel support 
Install packages from Adafruit, don't remember which.

### OpenCV
Install the requirements
```bash
sudo apt install build-essential cmake unzip pkg-config
sudo apt install libjpeg-dev libpng-dev libtiff-dev
sudo apt install libatlas-base-dev gfortran
sudo apt install python3-dev

wget -O opencv-4.1.0.zip https://github.com/opencv/opencv/archive/4.1.0.zip
wget -O opencv_contrib-4.1.0.zip https://github.com/opencv/opencv_contrib/archive/4.1.0.zip
unzip opencv-4.1.0.zip
unzip opencv_contrib-4.1.0.zip  
```

Compiling OpenCV
```bash
cd ~/opencv-4.1.0
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE \
      -D CMAKE_INSTALL_PREFIX=/usr/local \
      -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib-4.1.0/modules \
      -D ENABLE_NEON=ON \
      -D ENABLE_VFPV3=ON \
      -D BUILD_TESTS=OFF \
      -D OPENCV_ENABLE_NONFREE=ON \
      -D INSTALL_PYTHON_EXAMPLES=OFF \
      -D BUILD_EXAMPLES=OFF ..
make
sudo make install
sudo ldconfig
make clean
```


## PC application 
Install Qt5 for python3 for the PC application.