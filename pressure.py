
from enum import IntEnum
import collections

import smbus


PressureRawData = collections.namedtuple('PressureRawData', 'status pressure temperature')
PressureData = collections.namedtuple('PressureData', 'status pressure temperature')


class Reg(IntEnum):
    AutoIncrement = 0x80
    WhoAmI = 0x0f

    ResConf = 0x10

    Ctrl1 = 0x20

    Status = 0x27
    PressOutXL = 0x28
    PressOutL = 0x29
    PressOutH = 0x2A
    TempOutL = 0x2B
    TempOutH = 0x2C


class RegWhoAmI(IntEnum):
    Value = 0xBD


class RegResConf(IntEnum):
    MaskAvgP = 0x03
    AvgP_8 = 0x00
    AvgP_32 = 0x01
    AvgP_128 = 0x02
    AvgP_512 = 0x03
    MaskAvgT = 0x0C
    AvgT_8 = 0x00
    AvgT_32 = 0x04
    AvgT_128 = 0x08
    AvgT_512 = 0x0C


class RegCtrl1(IntEnum):
    BitActive = 0x80
    MaskOdr = 0x70
    Odr_OneShot = 0x00
    Odr_1Hz = 0x10
    Odr_7Hz = 0x20
    Odr_12_5Hz = 0x30
    Odr_25Hz = 0x40
    Bdu = 0x04


class RegStatus(IntEnum):
    T_DA = 0x01  # Temperature_DataAvailable
    P_DA = 0x02  # Pressure_DataAvailable
    T_OR = 0x10  # Temperature_OverRun
    P_OR = 0x20  # Pressure_OverRun


class Pressure:
    def __init__(self, bus, address=0x5d):
        self.chip_name = 'LPS25H'
        self.address = address
        self.bus = bus

    def setup(self, odr=RegCtrl1.Odr_7Hz):
        id = self.bus.read_byte_data(self.address, Reg.WhoAmI)
        if id != RegWhoAmI.Value:
            print('Failed to read id register, expected', RegWhoAmI.Value, 'but got', id)

        # Enable the specified ODR with block data update
        reg_value = (RegCtrl1.BitActive |
                     odr |
                     RegCtrl1.Bdu)
        self.bus.write_byte_data(self.address, Reg.Ctrl1, reg_value)

        # Enable max average
        reg_value = (RegResConf.AvgP_512 | RegResConf.AvgT_512)
        self.bus.write_byte_data(self.address, Reg.ResConf, reg_value)

    def read_values(self):
        data = self.bus.read_i2c_block_data(self.address,
                                            Reg.AutoIncrement + Reg.Status, 6)
        if len(data) != 6:
            print('Size of data read is not matching the expected format')
            return None
        inter_raw_data = PressureRawData(data[0],
                                         int.from_bytes(data[1:4], byteorder='little', signed=True),
                                         int.from_bytes(data[4:], byteorder='little', signed=True))
        status_flags = list()
        for bit in RegStatus:
            if inter_raw_data.status & bit != 0:
                status_flags.append(bit)
        inter_data = PressureData(tuple(status_flags),
                                  inter_raw_data.pressure / 4096,
                                  42.5 + inter_raw_data.temperature/480)
        return inter_data

    @staticmethod
    def estimate_altitude(hPa):
        return (1013.25 - hPa) / 0.12


