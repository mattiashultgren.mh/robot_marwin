
import queue
import threading
import time
import math
import sys
import random
from enum import IntEnum
sys.path.append('gui')

import PacketHandler
import tuples


def angle_diff(a1, a2):
    diff = (a1 - a2) % 360
    if diff > 180:
        diff -= 360
    return diff


def encoder_addition(encoder, value):
    return tuple(x + value for x in encoder)


def encoder_diff(encoder1, encoder2):
    return tuple(encoder1[i] - encoder2[i] for i in range(4))


def distance_to_encoder_steps(distance_cm):
    return int(distance_cm * 8)


class WeaponSpeeds(IntEnum):
    Off = 0
    Low = 48
    Medium = 100
    Max = 255


class MovementMode(IntEnum):
    Still = 0
    RotateToAngle = 1
    GoStraightToEncoderValue = 2


class MovementController:
    def __init__(self, serial_queue):
        self.mode = MovementMode.Still
        self.serial_queue = serial_queue
        self.old_speed = None

        self.speed = None
        self.goal_angle = None
        self.goal_encoders = None

    def action(self, imu_data, encoder_data):
        if self.mode == MovementMode.Still:
            pass
        elif self.mode == MovementMode.RotateToAngle:
            mag_angle = math.degrees(math.atan2(imu_data.mag_y, imu_data.mag_x))
            diff = angle_diff(mag_angle, self.goal_angle)
            print(self.mode, mag_angle, diff)

            if abs(diff) < 10:
                self.be_still()
            elif diff > 0:
                self.set_motor_speed(-self.speed, self.speed)
            else:
                self.set_motor_speed(self.speed, -self.speed)
        elif self.mode == MovementMode.GoStraightToEncoderValue:
            diff = sum(encoder_diff(encoder_data, self.goal_encoders)) / 4
            print(self.mode, diff)
            if diff < 0:
                self.be_still()
            else:
                self.set_motor_speed(self.speed, self.speed)

    def be_still(self):
        self.mode = MovementMode.Still
        self.set_motor_speed(0, 0)

    def rotate_to_angle(self, goal_angle, speed):
        print('Changing to rotate to angle')
        self.mode = MovementMode.RotateToAngle
        self.goal_angle = goal_angle
        self.speed = speed

    def move_forward(self, distance_cm, speed, current_encoder):
        print('Changing to GoStraightToEncoderValue')
        self.mode = MovementMode.GoStraightToEncoderValue
        self.goal_encoders = encoder_addition(current_encoder, -distance_to_encoder_steps(distance_cm))
        self.speed = speed

    def set_motor_speed(self, left_speed, right_speed):
        tmp = (left_speed, right_speed)
        if self.old_speed != tmp:
            self.old_speed = tmp
            self.serial_queue.put(Controller.get_motor_data(*self.old_speed))


class Controller(threading.Thread):
    def __init__(self, queues):
        super(Controller, self).__init__(target=self.run, kwargs=self)
        self.queues = queues
        self.in_queue = queues['controller']
        self.serial_queue = queues['serial']
        self.weapon_speed = WeaponSpeeds.Off
        self.distance_front_sensor = 0.0
        self.state_counter = 0
        self.state_start = time.time()
        self.state_function = None

        self.movement_controller = MovementController(queues['serial'])

        self.set_new_state_function(self.state_calibration)
        #self.set_new_state_function(self.movement_controller.be_still)
        #self.set_new_state_function(None)

        self.imu_data = None
        self.encoder_data = None
        self.vision_info = None

    def set_new_state_function(self, f):
        self.state_function = f
        self.state_counter = 0
        self.state_start = time.time()

    def run(self):
        while True:
            try:
                data = self.in_queue.get(timeout=0.05)
                if type(data) == PacketHandler.ReportEncoder:
                    self.encoder_data = (data.Motor0, data.Motor1, data.Motor2, data.Motor3)
                    print("Motor Encoders:",
                          data.Motor0,
                          data.Motor1,
                          data.Motor2,
                          data.Motor3)
                elif type(data) == PacketHandler.ReportVoltage:
                    if data.PacketType == PacketHandler.PacketType.ReportBattery:
                        print('Battery voltage:', data.Voltage)
                    elif data.PacketType == PacketHandler.PacketType.ReportDistanceVoltage:
                        self.distance_front_sensor = (0.5 * self.distance_front_sensor +
                                                      0.5 * Controller.distance_voltage_to_centimeter(data.Voltage))
#                        print('Distance voltage:', data.Voltage, self.distance_front_sensor)
                    else:
                        print('Controller got unknown voltage reported', data)
                elif type(data) == tuples.ImuData:
                    self.imu_data = data
                elif type(data) == tuples.VisionInformation:
                    self.vision_info = data
                else:
                    print('Unknown data sent to ControllerThread, ignored!', data)
            except queue.Empty:
                pass

            self.state_counter += 1
            if self.state_function is not None:
                self.state_function()
            self.movement_controller.action(self.imu_data, self.encoder_data)

    @staticmethod
    def distance_voltage_to_centimeter(voltage):
        ref_points = [(6450, 5.0),
                      (4900, 10.0),
                      (2850, 20.0),
                      (1900, 30.0),
                      (1400, 40.0),
                      (1150, 50.0)]
        if voltage > ref_points[0][0]:
            return ref_points[0][1]
        elif voltage < ref_points[-1][0]:
            return ref_points[-1][1]
        for idx in range(len(ref_points)):
            if voltage >= ref_points[idx+1][0]:
                diff = ref_points[idx][0] - ref_points[idx+1][0]
                point = voltage - ref_points[idx+1][0]
                p = point / diff
                return ref_points[idx][1] * p + (1.0-p) * ref_points[idx+1][1]
        raise Exception('Should not reach this point')

    def set_weapon_speed(self, speed):
        self.weapon_speed = speed
        self.serial_queue.put(tuples.WeaponControlData(int(self.weapon_speed), 0))

    def handle_weapon(self):
        if self.weapon_speed == WeaponSpeeds.Off:
            if self.distance_front_sensor < 40.0:
                self.set_weapon_speed(WeaponSpeeds.Low)
        elif self.weapon_speed == WeaponSpeeds.Low:
            if self.distance_front_sensor < 20.0:
                self.set_weapon_speed(WeaponSpeeds.Medium)
            elif self.distance_front_sensor > 48.0:
                self.set_weapon_speed(WeaponSpeeds.Off)
        elif self.weapon_speed == WeaponSpeeds.Medium:
            if self.distance_front_sensor < 10.0:
                self.set_weapon_speed(WeaponSpeeds.Max)
            elif self.distance_front_sensor > 25.0:
                self.set_weapon_speed(WeaponSpeeds.Low)
        elif self.weapon_speed == WeaponSpeeds.Max:
            if self.distance_front_sensor > 15.0:
                self.set_weapon_speed(WeaponSpeeds.Medium)

    @staticmethod
    def get_motor_data(left_speed, right_speed):
        pwms = [abs(left_speed), abs(right_speed), abs(left_speed), abs(right_speed)]
        dirs = [0 if left_speed > 0 else 1, 0 if right_speed > 0 else 1,
                1 if left_speed > 0 else 0, 1 if right_speed > 0 else 0]
        return tuples.MotorControlData(pwms, dirs)

    def state_calibration(self):
        if self.state_counter == 1:
            self.movement_controller.set_motor_speed(100, -100)
        elif (time.time() - self.state_start) > 10:
            self.movement_controller.set_motor_speed(0, 0)
            self.queues['i2c_handler'].put('calibration=False')
            self.set_new_state_function(self.state_north)

    def state_north(self):
        if self.state_counter == 1:
            self.movement_controller.rotate_to_angle(0, 100)
        elif self.movement_controller.mode == MovementMode.Still:
            self.set_new_state_function(self.state_attacking)

    def state_attacking(self):
        self.handle_weapon()

        if self.distance_front_sensor < 20.0:
            print('attacking by range info')
            self.movement_controller.move_forward(5, 255, self.encoder_data)
        elif self.distance_front_sensor < 40.0:
            print('attacking by range info')
            self.movement_controller.move_forward(5, 100, self.encoder_data)
        elif self.movement_controller.mode == MovementMode.Still:
            print(self.vision_info)
            floor = self.vision_info.floor[0] and self.vision_info.floor[1] > 50
            if False and floor and self.vision_info.opponent[0]:
                print('attacking by vision info')
                self.movement_controller.move_forward(5, 100, self.encoder_data)
            else:
                actions = ['rotate']
                if floor:
                    actions.append('move')
                if random.choice(actions) == 'move':
                    move_len = random.randint(10, int(self.vision_info.floor[1]))
                    print('moving forward')
                    self.movement_controller.move_forward(move_len, 100, self.encoder_data)
                else:
                    mag_angle = math.degrees(math.atan2(self.imu_data.mag_y, self.imu_data.mag_x))
                    goal_angle = mag_angle + random.choice([-135, -90, -45, 45, 90, 135])
                    print('new angle selected', mag_angle, goal_angle)
                    self.movement_controller.rotate_to_angle(goal_angle, 100)


