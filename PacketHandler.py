
import collections
from enum import IntEnum


def calc_chksum(data):
    reg = 0xffff
    for b in data:
        reg = (reg * 257 + int(b)) & 0xffff
    return reg


class Const(IntEnum):
    StartByte = 0xc4
    MinPacketSize = 6
    MaxPacketSize = 32


class PacketType(IntEnum):
    ReadBattery = 0
    ReportBattery = 1
    ReportEncoder = 2
    SetMotorSpeed = 3
    SetWeaponSpeed = 4
    ReportDistanceVoltage = 5


ReportVoltage = collections.namedtuple('ReportVoltage', 'PacketType SeqNum Voltage')
ReportEncoder = collections.namedtuple('ReportEncoder', 'PacketType SeqNum Motor0 Motor1 Motor2 Motor3')


class Deserializer:
    def __init__(self):
        self.data = b''
        self.expected_size = 0
        self.checksum = 0

    def clear(self):
        self.data = b''

    def add_byte(self, data):
        self.data += data

        if len(self.data) == 1:
            if self.data[0] != Const.StartByte:
                self.data = b''
                print('ignoring data', data)
        elif len(self.data) == 2:
            self.expected_size = int(self.data[-1])
            if self.expected_size > Const.MaxPacketSize:
                print('Length field too big!')
                self.data = b''

        if len(self.data) >= Const.MinPacketSize and len(self.data) == self.expected_size:
            chksum = int.from_bytes(self.data[-2:], 'little')
            calced_chksum = calc_chksum(self.data[:-2])
            if chksum == calced_chksum:
                return True
            else:
                print('wrong check sum, dropping', str(len(self.data)), 'bytes')
                print('rxd', chksum, 'calced', calced_chksum)
                self.data = b''
                return False
        else:
            return False

    def get_packet(self):
        if self.data[3] == PacketType.ReportBattery:
            if len(self.data) == 8:
                return ReportVoltage(PacketType.ReportBattery,
                                     int(self.data[2]),
                                     int(self.data[4]) + 256*int(self.data[5]))
        elif self.data[3] == PacketType.ReportDistanceVoltage:
            if len(self.data) == 8:
                return ReportVoltage(PacketType.ReportDistanceVoltage,
                                     int(self.data[2]),
                                     int(self.data[4]) + 256 * int(self.data[5]))
        elif self.data[3] == PacketType.ReportEncoder:
            if len(self.data) == 22:
                return ReportEncoder(PacketType.ReportEncoder,
                                     int(self.data[2]),
                                     int.from_bytes(self.data[4:8], 'little', signed=True),
                                     int.from_bytes(self.data[8:12], 'little', signed=True),
                                     int.from_bytes(self.data[12:16], 'little', signed=True),
                                     int.from_bytes(self.data[16:20], 'little', signed=True))
        return None


class Serializer:
    CurrentTxSeqNumber = 0

    @staticmethod
    def read_battery():
        data = bytes([Const.StartByte, 6, Serializer.CurrentTxSeqNumber, PacketType.ReadBattery])
        Serializer.CurrentTxSeqNumber = (Serializer.CurrentTxSeqNumber + 1) % 256
        data += calc_chksum(data).to_bytes(2, 'little')
        return data

    @staticmethod
    def set_motor_speed(pwm, dir):
        data = bytes([Const.StartByte, 14, Serializer.CurrentTxSeqNumber, PacketType.SetMotorSpeed] +
                     list(pwm) + list(dir))
        Serializer.CurrentTxSeqNumber = (Serializer.CurrentTxSeqNumber + 1) % 256
        data += calc_chksum(data).to_bytes(2, 'little')
        return data

    @staticmethod
    def set_weapon_speed(pwm, dir):
        data = bytes([Const.StartByte, 8, Serializer.CurrentTxSeqNumber, PacketType.SetWeaponSpeed, pwm, dir])
        Serializer.CurrentTxSeqNumber = (Serializer.CurrentTxSeqNumber + 1) % 256
        data += calc_chksum(data).to_bytes(2, 'little')
        return data

